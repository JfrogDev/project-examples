#!/bin/bash -ex

# ARTIFACTORY_USERNAME      (Required) Username for Artifactory instance configured in pom.xml
#                               Example: "jainishs"
# ARTIFACTORY_PASSWORD      (Required) Password for Artifactory instance configured in pom.xml
# ARTIFACTORY_CONTEXT_URL   (Required) URL for Artifactory
# buildNumber               (Optional) User-visible identification of this build
#                               Example: "12345"
if [ -z "$buildNumber" ]; then
	buildNumber=`date +%s`
fi

mvn install -DskipTests=true
mvn test
mvn deploy -Dusername=${ARTIFACTORY_USERNAME} -Dpassword=${ARTIFACTORY_PASSWORD} -Durl=${ARTIFACTORY_CONTEXT_URL}
