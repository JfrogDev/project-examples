# **DEPRECATED** - This pipe will stop working after 1st May 2021. Instead, please use a more generic pipe available here [https://bitbucket.org/jfrog/jfrog-setup-cli/src/master/](https://bitbucket.org/jfrog/jfrog-setup-cli/src/master/).

Sample projects for training and testing CI setup with Artifactory
